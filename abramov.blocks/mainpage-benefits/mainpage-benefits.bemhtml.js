block('mainpage-benefits')(content()(function(){

  return [{
      block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block: 'icon', mods: {'ico-opyt': true, cls: 'icon-url'}} },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<b>Серьезный опыт</b><p>Более 20 лет адвокатской практики</p>'} }
              ]
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block: 'icon', mods: {'ico-rezultat': true, cls: 'icon-url'}} },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<b>Серьезный опыт</b><p>Более 20 лет адвокатской практики</p>'}}
              ]
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block: 'icon', mods: {'ico-nagrada': true, cls: 'icon-url'}} },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<b>Награды</b> <p>«Призер профессионального конкурса Лучший центр правовой помощи»</p>'}}
              ]
            }
        }]
      }]
}));
