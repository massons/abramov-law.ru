block('item-top-block__icons')({
      mix: {block:'decoratoer', mods:{hide: 'mob' }},
      js:true,
      content:
         [
            {elem: 'item',
             content:
                {block:'link', url:'#', mods:{theme:'brown',block:true},
                  content:
                  {block: 'row',
                    content:[
                      {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-arbitrazhnye-sudy':true},cls : 'icon-url' } },
                      {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Представительство интересов в Арбитражных судах</p>'} }
                    ]
                  }
                }
            },
            {elem: 'item',
             content:
              {block:'link', url:'#', mods:{theme:'brown',block:true},
                  content:
                  {block: 'row',
                    content:[
                      {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-arendnye-spory':true},cls : 'icon-url' } },
                      {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Арендные споры</p>'}}
                    ]
                  }
                }
            }
        ]
});
