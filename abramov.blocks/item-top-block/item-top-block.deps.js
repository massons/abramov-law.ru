({
    shouldDeps: [
      { block: 'item-top-block', elem:'form'},
      { block: 'item-top-block', elem:'icons'},
      { block: 'price-block'},
      { block: 'row' },
      { block: 'link', mods:{'theme':'brown'} },
      { block: 'input', mods:{'theme':'light-brown'} },
      { block: 'decorator' },
      { block: 'image' },
      { block: 'input' },
      { block: 'icon' },
      { block: 'form' },
      { block: 'form', mods:'message'},
      { block: 'popup' },
      { block: 'form-popup' }
    ]
})
