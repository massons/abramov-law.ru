block('item-top-block__form')(addJs()(false),content()(function(){

return [
  {
    block:'form',
    tag:'form',
    js:true,
    mods: {
        /* 'has-validation' : true, */
        message: 'popup'
    },
    method: 'GET',
    attrs: { url:'/'},
    content:
      { block: 'row',
        mods: {vam: 's'},
        content:
          [
            { elem: 'col', elemMods: { sw: 12, mw: 12, lw: 6}, mix:[{block:'text', mods:{align:'center'}}],
              content:
                {
                  block: 'input',
                  js: true,
                  /* 'has-validation': true, */
                  mods: { theme:'light-brown', size: 'm', required : true, message: 'text'},
                  attrs: { required: true },
                  placeholder: '+7 (   ) ___ __ __',
                  name: 'phone',
                  val: ''
                }
            },
            { elem: 'col', elemMods: { sw: 12, mw: 12, lw: 6 }, mix:[{block:'text', mods:{align:'center'}}],
              content:
                {
                  block: 'button',
                  mods: {theme:'brown', size:'m', type: 'submit'},
                  mix: {block:'decorator', mods:{'indent-b':'xs', 'indent-t':'xs'}},
                  id: 'btn-order-item',
                  content:
                    { block:'font',
                      mods: { size: 'auto'},
                      content: 'Получить консультацию!'
                    }
                }
            }
          ]
      }
  }
]
}));
