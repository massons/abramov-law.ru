modules.define('item-top-block__form', ['i-bem-dom', 'form'], function(provide, bemDom, Form) {

  provide(bemDom.declElem('item-top-block', 'form', {
    onSetMod: {
        js: {
            inited: function() {

              this._form = this._form = this.findChildBlock(Form);

              this._form._domEvents().on('submit', function(e, val) {

                console.log('submit',this._form)

                // e.preventDefault();
                // return false;
                  // this._form.validate()
                  //     .then(function (fieldsStatuses) {
                          // if(this._form.checkFields(fieldsStatuses)) {
                          //     this._form.getMessage().hide();
                          //     console.log(val);
                          // } else {
                          //     this._form.setMessageVal(this._concatMessages(fieldsStatuses));
                          //     this._form.getMessage().show();
                          //     this._form.getInvalidFields()
                          //         .then(function (invalidFields) {
                          //             invalidFields[0].getControl().setMod('focused');
                          //         });
                          // }
                      // }.bind(this));

              }.bind(this));
              //
            }
        }
    },


  _concatMessages : function(fieldsStatuses) {
      var messages = [];
      for(var i = 0, l = fieldsStatuses.length; i < l; i++) {
          if(fieldsStatuses[i] !== null) {
              messages.push([
                  fieldsStatuses[i]['field'],
                  ': ',
                  fieldsStatuses[i]['message']
              ].join(''));
          }
      }
      return messages.join('<br>');
  }

  }));

});
