block('item-top-block')(addJs()(false),content()(function(){

  return [
        { block: 'row',
          mods: {vam: 's'},
          content:
            [
              { elem: 'col',
                elemMods: { sw: 12, mw: 6 },
                mix:{block:'text', mods:{'align':'center'}},
                content:{
                  block:'image',
                  url: '../../img/usluga.jpg',
                  title: 'Адвокат Абрамов Михаил Васильевич',
                  mods: { w: '100procent' }
                }
              },
              { elem: 'col',
                elemMods: { sw: 12, mw: 6 },
                mix:[{block:'decorator', mods:{'indent-a':'h'}}],
                content:[
                  {block:'h1', tag:'h1', mix:[{block:'decorator', mods:{'indent-t':'none'}}], content:'Защита прав потребителей'},
                  {block:'text', mods:{align:'left'}, mix:[{block:'mainpage-about', elem:'text'}],
                    content: [
                      {html:'<p>Профессиональная юридическая помощь и консультация адвоката по вопросам защиты прав потребителей. Всегда заключаем договор и несем ответственность за результат</p>'},
                      {block:'item-top-block', elem:'form'}
                    ]
                  }
                ]
              }
            ]
        },
        { block: 'row',
          mods: {vam: 's'},
          content:
              { elem: 'col',
                elemMods: { sw: 12 },
                content: { block:'item-top-block', elem:'icons' }
              }
        },
        { block: 'row',
          mods: {vam: 's'},
          content:
              { elem: 'col',
                elemMods: { sw: 12 },
                content: { block:'price-block' }
              }
        }
  ]
}));
