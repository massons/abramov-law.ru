modules.define('price-block', ['i-bem-dom', 'link', 'modal'], function(provide, bemDom, Link, Modal) {

  provide(bemDom.declBlock(this.name, {
      onSetMod: {
          'js': {
              'inited' : function() {

                var link = this.findChildBlock(Link),
                    modal = this.findChildBlock(Modal);

                    this._events(Link).on('click', function() {
                        modal.setMod('visible', true);
                    });

              }
          }
      }
  }));

});
