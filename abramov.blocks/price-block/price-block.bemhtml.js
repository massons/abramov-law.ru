block('price-block')(addJs()(false),content()(function(){

return [
  { elem:'wrapp',
    content:[
      {
          block : 'modal',
          mods : { theme : 'islands', autoclosable : true },
          content : {block: 'form-popup'},
      },
      {
        block: 'table',
        tag: 'table',
        mods: {'mobile-scroll': true},
        attrs: { style: 'table-layout: fixed; width: 100%;' },
        content: [
          {
            elem: 'row',
            tag: 'tr',
            content: [
                { elem: 'cell', tag: 'td', content:'Взыскание неустойки за несвоевременню сдачу застройщиком квартиры'},
                { elem: 'cell', tag: 'td', mix:{ block:'text', mods:{'align':'right'}}, content:'от 51 000 руб' },
                { elem: 'cell', tag: 'td', mix:{ block:'text', mods:{'align':'right'}},
                  content:
                  [
                    {
                        block: 'link',
                        mods: {
                            pseudo: true,
                            theme: 'islands'
                        },
                        content: 'Открыть модальное окно'
                    },


                  ]
                }
            ]
          },
          {
           elem: 'row',
           tag: 'tr',
           content: [
               { elem: 'cell', tag: 'td', content:'Взыскание неустойки за несвоевременню сдачу застройщиком квартиры'},
               { elem: 'cell', tag: 'td', mix:{ block:'text', mods:{'align':'right'}}, content:'от 50 000 руб' },
               { elem: 'cell', tag: 'td', mix:{ block:'text', mods:{'align':'right'}},
                  content:
                  {block: 'link', mods:{pseudo: true}, content:'Заказать услугу'}
             }
           ]
          },
          {
           elem: 'row',
           tag: 'tr',
           content: [
               { elem: 'cell', tag: 'td', size: 's', content:'Взыскание неустойки за несвоевременню сдачу застройщиком квартиры'},
               { elem: 'cell', tag: 'td', size: 'm', mix:{ block:'text', mods:{'align':'right'}}, content:'от 50 000 руб' },
               { elem: 'cell', tag: 'td', size: 'l', mix:{ block:'text', mods:{'align':'right'}}, content:'Заказать услугу'}
           ]
          },
          {
           elem: 'row',
           tag: 'tr',
           content: [
               { elem: 'cell', tag: 'td', size: 's', content:'Взыскание неустойки за несвоевременню сдачу застройщиком квартиры'},
               { elem: 'cell', tag: 'td', size: 'm', content:'от 50 000 руб' },
               { elem: 'cell', tag: 'td', size: 'l', content:'Заказать услугу'}
           ]
          }
         ]
     }
   ]
  }
 ]
}));
