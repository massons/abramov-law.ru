({
    shouldDeps: [
      { block: 'row' },
      { block: 'link', mods:{'theme':'brown'} },
      { block: 'input', mods:{'theme':'light-brown'} },
      { block: 'decorator' },
      { block: 'image' },
      { block: 'input' },
      { block: 'table' },
      { block: 'form' },
      { block: 'form-popup' },
      { block: 'popup' },
      { block: 'modal', mods: {autoclosable: true,theme: 'islands'}},
      { block: 'page', mods: { theme: 'islands'}}
    ]
})
