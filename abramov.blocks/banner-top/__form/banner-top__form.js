modules.define('banner-top__form', ['i-bem-dom', 'form'], function(provide, bemDom, Form) {

  provide(bemDom.declElem('banner-top',
    'form',
    {
      onSetMod: {
          js: {
              inited: function() {
                this._form = this;

                                // return false;
                // console.log('this', this._form);

                this._form._domEvents().on('submit', function(e, val) {
                  e.preventDefault()

                    // this._form.validate()
                    //     .then(function (fieldsStatuses) {
                    //         if(this._form.checkFields(fieldsStatuses)) {
                    //             this._form.getMessage().hide();
                    //             console.log(val);
                    //         } else {
                    //             this._form.setMessageVal(this._concatMessages(fieldsStatuses));
                    //             this._form.getMessage().show();
                    //             this._form.getInvalidFields()
                    //                 .then(function (invalidFields) {
                    //                     invalidFields[0].getControl().setMod('focused');
                    //                 });
                    //         }
                    //     }.bind(this));
                }.bind(this));

                return false;
              }
          }
      },


    _concatMessages : function(fieldsStatuses) {
        var messages = [];
        for(var i = 0, l = fieldsStatuses.length; i < l; i++) {
            if(fieldsStatuses[i] !== null) {
                messages.push([
                    fieldsStatuses[i]['field'],
                    ': ',
                    fieldsStatuses[i]['message']
                ].join(''));
            }
        }
        return messages.join('<br>');
    }
  }))

});
