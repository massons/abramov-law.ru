block('banner-top').elem('form')({
    tag: 'form',
    js: true,
    name: 'banner-mainpage',
    mods: {
        'has-validation': true,
        message: 'form-fast'
    },
    method: 'GET',
    content: [
    {block: 'row',
      content: {
        elem: 'col',
    elemMods: {'sw':12},
    mix:[{block: 'text', mods:{align:'right'}}],
     content: [
              { block: 'banner-top',
                elem: 'title',
                tag: 'h2',
                mods: {size: 'xxxxl', color:'white'},
                mix: {block: 'decorator', mods:{'indent-v': 'xxl', 'indent-h': 'none'} },
                content: {html: 'Квалифицированная<br> юридическая помощь<br> адвокатов'}
              },
              {
                block: 'font',
                tag: 'h2',
                mods: {size: 'xl', color:'white'},
                content: {html:'Представлю Ваши интересы <br> и смогу успешно задитить в суде'}
              },
              { block: 'input',
                js: {
                   required:{
                     message:'FUCK YOU!'
                   }
                },
                mods: { theme:'brown', size: 'm'},
                direction: ['top-left'],
                attrs: { required: true },
                placeholder: '+7 (___) ___ __ __',
                name: 'phone',
                val: ''
              },
              { block: 'button',
                mods: {theme:'brown', size:'w250px', type:'submit'},
                mix: {block:'decorator', mods:{'indent-v':'xs'}},
                id: 'btn-order',
                content:
                    { block: 'font',
                       mods: { size: 'auto', color: 'white' },
                    content: 'Получить консультацию'
                    }
              }
            ]
          }
        }
    ]
});
