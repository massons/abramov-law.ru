({
    shouldDeps: [
      {block: 'banner-top', elem: 'form'},
      {block: 'decorator'},
      {block: 'image'},
      {block: 'text'},
      {block: 'font'},
      {block: 'form'},
      {block: 'button'},
      {block: 'input'}
  ]
})
