block('mainpage-category__fiz')(content()(function(){

return [

    {block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                      {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-avtoyurist':true}, cls : 'icon-url' } },
                      {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Консультации адвоката</p>'} }
                    ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-bankrotstvo':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по деловому строительству</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-dolevoe-stroitelstvo':true}, cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по банкроству</p>'}}
              ]
            }
          }
      }
    ]},
    {block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-ehkonomicheskie-prestupleniya':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по трудовым спорам</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-grazhdanskie-spory':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по гражданским спорам</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-ispolnitelnoe-proizvodstvo':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Исполнительное производство</p>'}}
                ]
              }
            }
        }]
    },
    {block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-konsultacii-advokata':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Консультации адвоката</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-nasledstvennye-spory':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по наследственным спорам</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-sostavlenie-iskov':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Составление исков, договоров и др.документов</p>'}}
                ]
              }
            }
        }]
    },
    {block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-trudovye-spory':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Трудовые споры</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-zashchita-prav-potrebitelej':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Защита прав потребителей</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-zemelnye-spory':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Земельные споры</p>'}}
                ]
              }
            }
        }]
    },
    {block: 'row',
      mods: {vam: 's'},
      content: [
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-sdelki-s-nedvizhimostyu':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Сопровождение сделок с недвижимостью</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-zhilishchnye-spory':true}, cls : 'icon-url' } },
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Адвокат по сеймейным спорам</p>'}}
                ]
              }
            }
        },
        {elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          content:
            {block:'link', url:'#', mods:{theme:'brown',block:true},
              content:
              {block: 'row',
                content:[
                  {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-fiz-semejnye-spory':true}, cls : 'icon-url' }},
                  {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Жилищьные споры</p>'}}
                ]
              }
            }
        }]
    }
    ]
}));
