({
    shouldDeps: [
      { block: 'icon' },
      { block: 'row' },
      { block: 'link', mods:{'theme':'brown'} },
      { block: 'page', mods:{'display':'none'} },
      { block: 'font', mods:{'weight':'bold'} },
      { block: 'radio-group' },
      { block: 'button' },
      { block: 'decorator' },
      { block: 'mainpage-category', elem:'fiz' },
      { block: 'mainpage-category', elem:'ur' }
    ]
})
