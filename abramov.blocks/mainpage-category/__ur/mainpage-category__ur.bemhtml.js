block('mainpage-category__ur')(content()(function(){

return [
  {
    block: 'row',
    mods: {vam: 's'},
    content: [
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-arbitrazhnye-sudy':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Представительство интересов в Арбитражных судах</p>'} }
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-arendnye-spory':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Арендные споры</p>'}}
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-dogovornye-spory':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Договорные споры</p>'}}
              ]
            }
          }
      }]
    },
  {
    block: 'row',
    mods: {vam: 's'},
    content: [
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-ispolnitelnoe-proizvodstvo':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Помощь по исполнительному производству</p>'} }
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-kadastrovaya-stoimost':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Оспаривание кадастровой стоимости</p>'}}
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-osparivanie-dejstvij-admin':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Оспаривание действий и решений Администрации</p>'}}
              ]
            }
          }
      }]
    },
  {
    block: 'row',
    mods: {vam: 's'},
    content: [
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-podgotovka-korp-dokumentov':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Подготовка корпоративных документов</p>'} }
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-soprovozhdenie-bankrotstva':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Сопровождение процедуры банкротства</p>'}}
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-soprovozhdenie-nalog-proverok':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Сопровождение налоговых проверок</p>'}}
              ]
            }
          }
      }]
    },
  {
    block: 'row',
    mods: {vam: 's'},
    content: [
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-soprovozhdenie-sdelok':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Сопровождение сделок</p>'} }
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-subsidiarnaya-otvetstvennost':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Помощь при субсидиарной ответственности</p>'}}
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-vzyskanie-debit-zadolzhennosti':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Взыскание дебиторской задолженности</p>'}}
              ]
            }
          }
      }]
    },
  {
    block: 'row',
    mods: {vam: 's'},
    content: [
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-zashchita-biznesa':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Защита бизнеса</p>'} }
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-zashchita-pri-proverkah':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Защита организации при проверках</p>'}}
              ]
            }
          }
      },
      {elem: 'col',
        elemMods: { sw: 12, mw: 4 },
        content:
          {block:'link', url:'#', mods:{theme:'brown',block:true},
            content:
            {block: 'row',
              content:[
                {elem: 'col', elemMods: { sw: 3 }, mix:[{block: 'decorator', mods:{'space-v':'l'}},{block: 'text', mods:{'align':'center'}}], content: {block:'icon', mods:{'ico-ur-zashchita-reputacii':true},cls : 'icon-url' } },
                {elem: 'col', elemMods: { sw: 9 }, mix:{block: 'decorator', mods:{'space-v':'l'}}, content: {html:'<p>Защита деловой репутации организации</p>'}}
              ]
            }
          }
      }]
    }
  ]

}));
