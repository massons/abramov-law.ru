modules.define('mainpage-category', ['i-bem-dom','button'], function(provide, bemDom, Button) {

    provide(bemDom.declBlock(this.name, {
          onSetMod: {
              js: {
                  inited: function() {
                      // Finded all items
                      var btn_fiz = this.findChildBlocks(Button)._entities[0];
                      var btn_ur = this.findChildBlocks(Button)._entities[1];
                      var fiz = this.findChildElem({ elem:'fiz'});
                      var ur = this.findChildElem({ elem:'ur'});

                      //Set prog
                      btn_fiz.setMod('checked');
                      btn_ur._domEvents().on('click', function() {
                        console.log('btn_ur',btn_ur);
                           if(ur.hasMod('disabled')){
                             ur.delMod('disabled');
                             fiz.setMod('disabled');
                             btn_ur.setMod('checked');
                             btn_fiz.delMod('checked');
                           }
                      });

                      btn_fiz._domEvents().on('click', function() {
                           if(fiz.hasMod('disabled')){
                             fiz.delMod('disabled');
                             ur.setMod('disabled');
                             btn_ur.delMod('checked');
                             btn_fiz.setMod('checked');
                           }
                      });

                  }
              }
          }
        }
      )
    )

});
