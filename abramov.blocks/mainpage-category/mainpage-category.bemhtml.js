block('mainpage-category')(addJs()(true),content()(function(){

return [
  { block: 'row',
    mods: {vam: 's'},
    content:
      [{ elem: 'col',
          elemMods: { sw: 12 },
          mix:{block:'text', mods:{'align':'center'}},
          content:{html:'<h2>ОБЛАСТИ ДЕЯТЕЛЬНОСТИ</h2>'}
        },
        { elem: 'col',
          elemMods: { sw: 12 },
          mix:[{block:'text', mods:{'align':'center'}}, {block:'decorator', mods:{'indent-a':'s'}}],
          content:[
              { block:'button', mods:{'theme':'brown', size:'xl'}, mix:[{block:'font', mods:{'weight':'bold'}},{block:'decorator',mods:{'indent-a':'s'}}], content:'ДЛЯ ФИЗИЧЕСКИХ ЛИЦ'},
              { block:'button', mods:{'theme':'brown', size:'xl', 'active':true}, mix:[{block:'font', mods:{'weight':'bold'}},{block:'decorator',mods:{'indent-a':'s'}}], content:'ДЛЯ ЮРИДИЧЕСКИХ ЛИЦ'},
            ]
      },
      { elem: 'col',
        elemMods: { sw: 12 },
        content:[
            {block:'mainpage-category', elem:'fiz'},
            {block:'mainpage-category', elem:'ur', elemMods:{'disabled':true}}
          ]
      }]
    }]
}));
