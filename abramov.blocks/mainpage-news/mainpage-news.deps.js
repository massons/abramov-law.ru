({
    shouldDeps: [
      { block: 'row' },
      { block: 'link', mods:{'theme':'brown'} },
      { block: 'font', mods:{'weight':'bold'} },
      { block: 'decorator' },
      { block: 'image' }
    ]
})
