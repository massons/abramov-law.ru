block('mainpage-about')(addJs()(false),content()(function(){

return [

      { block: 'row',
        mods: {vam: 's'},
        content:
          [{ elem: 'col',
              elemMods: { sw: 12, mw: 6},
              mix:{block:'text', mods:{'align':'center'}},
              content:{
                block:'image',
                url: '../../img/Abramov.jpg',
                title: 'Адвокат Абрамов Михаил Васильевич',
                mods: { w: '100procent' },
                mix:{block:'decorator', mods:{'space-r':'xxl'}},
              }
            },
            { elem: 'col',
              elemMods: { sw: 12, mw: 6 },
              mix:[{block:'text', mods:{'align':'center'}}, {block:'decorator', mods:{'indent-a':'s'}}],
              content:[
                {block:'title', mix:{block:'font', mods:{color:'light-brown'}}, tag:'h2', content:'Обо мне'},
                {block:'text', mods:{align:'left'}, mix:[{block:'mainpage-about', elem:'text'},{block:'font', mods:{color:'light-brown'}}], content: {
                  html:'<p>Окончил Академию права и управления Министерства юстиции РФ в 2003 году по специальности «Юриспруденция» с присвоением квалификации «Юрист».</p><p>В 2009 году прошел дополнительное обучение в Институте экономики и предпринимательства г. Москвы по курсу «Налоговое консультирование» с присвоением квалификации «Налоговый консультант».</p><p>До приобретения статуса адвоката работал юристом в консалтинговой компании, специалистом юридического отдела в налоговой инспекции, заместителем руководителя юридического департамента в управляющей компании, руководителем юридической службы в торговой компании.</p><p>Состою в адвокатской палате Московской области с 2011 года, регистрационный номер 50/6763 в реестре адвокатов Московской области.</p><p>Более 600 успешных дел</p>'
                  }
                }
                ]
           }]
        }

]
}));
