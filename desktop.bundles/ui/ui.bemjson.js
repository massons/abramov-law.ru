module.exports = {
    block: 'page',
    title: 'Page ui',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css' },
        { elem: 'css', url: 'ui.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'ui.min.js' },
                { elem: 'js', url: 'https://code.jquery.com/jquery-3.4.1.min.js' },
                { elem: 'js', url: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js' },
                { elem: 'js', url: 'https://api-maps.yandex.ru/2.1/?apikey=a53f98ff-d9e2-4b52-8e68-86bc2cb7f762&lang=ru_RU' },
                { elem: 'js', url: '../../libs/action.js' }
            ],
    mods: { theme: 'islands' },
    content: [
      {block: 'header'},
      {
          block : 'form',
          mods : {
              'has-validation' : true,
              message : 'text'
          },
          method : 'GET',
          content : [
              {
                  block : 'form-field',
                  id : 'input',
                  mods : {
                      type : 'input',
                      'has-validation' : true,
                      required : true,
                      message : 'text'
                  },
                  content : [
                      {
                          block : 'label',
                          content : 'Input'
                      },
                      {
                          block : 'input',
                          name : 'firstName'
                      }
                  ]
              }
          ]
      },
      {block: 'footer'}
 ]
};
