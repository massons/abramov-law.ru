module.exports = {
    block: 'page',
    title: 'Новости',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: 'Описание' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css' },
        { elem: 'css', url: 'news.min.css' },
    ],
    scripts: [
      { elem: 'js', url: 'news.min.js' },
      { elem: 'js', url: 'https://code.jquery.com/jquery-3.4.1.min.js' },
      { elem: 'js', url: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js' },
      { elem: 'js', url: '../../libs/action.js' }
    ],
    mods: { theme: 'brown'},
    mix: { block:'decorator', mods:{'indent-a':'auto'}},
content: [
  {block:'header'},
  {block:'breadcrumbs'},
  {block: 'row',
    content:{
      elem: 'col',
      content:{
          block: 'content',
          content: [
            { block: 'news' }
          ]
        }
      }
  },
  {block:'footer'}
]
};
