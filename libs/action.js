$(function() {
    'use strict';

    // $('.sidebar .ul_hidden_toggle').eq(0).hide();
    // $('.sidebar .li_link_toggle .image').eq(1).addClass('rotate');
    //
    // $('.sidebar .li_link_toggle').click(function(event) {
    //     $(this).find('.image').toggleClass('rotate');
    //     $(this).find('.ul_hidden_toggle').toggle(500);
    // });

    /* Форма на главной * /
    if($('#form_order', document)[0]){
      // КЛИК ПО КНОПКЕ ОТПРАВКА
      $(document).on('click', '#form_order_btn_send', function(event){
        event.preventDefault(); // делаем отмену действия браузера и формируем ajax

        var formData = new FormData( $('#form_order')[0] );
        console.log('formData: ', formData);

        if( $('#agree', document)[0]){

          $.ajax({
            url: '/send-order/',
            type: 'post',
            data: formData,
            processData: false,
            // dataType: "json",
            success: function(result) {
              console.log(result)
              // window.location = "/tnx-order/"
            }
          });

        }else{
          alert('Вы не согласились с обработкой персональных данных')
        }

        return false;
      })
    }
    /* \Форма на главной */

    /* call order https://github.com/kylefox/jquery-modal * /
    if($('.js-call-order', document)){

        $(document).on('click', '.js-call-order', function(event){
            event.preventDefault();
            $.get('/__/form-call-order.html', function(data){

                $.when(
                    $('.form.consult').remove()
                ).then(
                    $('body').append(data)
                );
                setTimeout(function() {
                    $('#form-call-order', document).modal({fadeDuration: 200});
                }, 100);

            })
        });

        $(document).on('click', '#btn-call-order', function(event){
            $('#form-call-order', document).submit();
        });

    }
    /* \call order */

    /* order page order * /
    if($('#btn-order', document)){

        $(document).on('click', '#btn-order', function(event){
            event.preventDefault();
            $.get('/__/form-usluga-order.html', function(data){

                $.when(
                    $('#form_order').remove()
                ).then(
                    $('body').append(data)
                );
                setTimeout(function() {
                    $('#form-usluga-order', document).modal({fadeDuration: 200});
                }, 100);

            })
        });

        $(document).on('click', '#form_order_btn_send', function(event){

            var form = $('#form-usluga-order', document),
                complite = true;

            //length input name
            if($($(form)[0][6]).val().length < 2){
                $($(form)[0][6]).css({'border-color': '#f00'});
                complite = false;
            }else{
                $($(form)[0][6]).css({'border-color': '#d9d9d9'});
            }

            // phone input
            if($($(form)[0][7]).val().length < 10) {
                $($(form)[0][7]).css({'border-color': '#f00'});
                complite = false;
            }else{
                $($(form)[0][7]).css({'border-color': '#d9d9d9'});
            }

            //your email
            if($($(form)[0][8]).val().length < 7) {
                $($(form)[0][8]).css({'border-color': '#f00'});
                complite = false;
            }else{
                $($(form)[0][8]).css({'border-color': '#d9d9d9'});
            }

            // agree
            if(!$($(form)[0][10]).prop("checked") ){
                $($(form)[0][10]).parent().css({'border': '1px solid #f00'});
                complite = false;
            }else{
                $($(form)[0][10]).parent().css({'border': 'none'});
            }

            if(complite) {
                $('#form-usluga-order', document).submit();
            }else{
                return false;
            }
        });
    }
    /* \order page order */




})
