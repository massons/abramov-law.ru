({
    shouldDeps: [
      { block: 'image' },
      { block: 'icon' },
      { block: 'logo' },
      { block: 'link' },
      { block: 'text' }
    ]
})
