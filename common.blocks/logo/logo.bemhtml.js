block('logo').content()(function() {
    return [{
      block: 'link',
      mix:{block:'page', mods: {'display':'block'} },
      url:'/',
      content:{
        block: 'image',
        url: '../../img/logo/logo-brown.png',
        title: 'Адвокат Абрамов Михаил Васильевич',
        mods: { padding: '10' }
      }
    }
  ]
});
