block('news').content()(function() {
    return[
      {
        elem:'wrapp',
        elemMods: {type:'vertikal'},
        content:[
          {
            elem:'item',
            mix:[{block: 'decorator', mods:{'space-b':'xl'}}],
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  mix:[{block: 'decorator', mods:{'space-l':'xl'}}],
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          },
          {
            elem:'item',
            mix:[{block: 'decorator', mods:{'space-b':'xl'}}],
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  mix:[{block: 'decorator', mods:{'space-l':'xl'}}],
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          }
        ]
      },
      {
        elem:'wrapp',
        elemMods: {type:'horizontal'},
        content:[
          {
            elem:'item',
            elemMods:{type:'vertikal'},
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          },
          {
            elem:'item',
            elemMods:{type:'vertikal'},
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          },
          {
            elem:'item',
            elemMods:{type:'vertikal'},
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          },
          {
            elem:'item',
            elemMods:{type:'vertikal'},
            content:
              [
                {elem:'img',  content: { block:'image', url:'../../img/news1.jpg', title: 'Адвокат Абрамов Михаил Васильевич'}},
                {elem:'info',
                  content:
                  [
                    {elem:'date', mix:[{block:'font', mods:{size:'s'}}, {block: 'page', mods:{'bg-color':'light-brown'}}], content: '01.01.2022'},
                    {elem:'title',mix:[{block: 'font',mods:{size:'xl'}}],content: 'Защита деловой репутации в арбитражном суде'},
                    {elem:'desc', content: 'Для любого предпринимателя или организации, которые работают в определённой сфере и планируют работать там продолжительное время'},
                    {elem:'link', content: { block:'link', mods:{theme: 'brown'}, url:'#news_more', content:'Читать полностью'}}
                  ]
                }
              ]
          }
        ]
      }
    ];
});
