({
  shouldDeps: [
    { block: 'theme' },
    { block: 'button' },
    { block: 'button', mods:{'theme': 'brown'} },
    { block: 'logo' },
    { block: 'libs' },
    { block: 'form' },
    { block: 'table'},
    { block: 'tabs' },
    { block: 'how-working' },
    { block: 'form-fast' },
    { block: 'link' },
    { block: 'link', mods:{theme:'light-brown'} },
    { block: 'text' },
    { block: 'icon' },
    { block: 'icon', mods: { glyph: 'twitter' } }
  ]
})
