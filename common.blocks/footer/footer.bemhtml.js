block('footer')({
  mix: [{block:'theme', mods:{'color': 'bg-brown' }}, {block:'decorator', mods:{'space-v':'xxxl'}}],
  content:
  [
    {
      block: 'row',
      content: [
          {
            elem: 'col',
            elemMods: { sw: 0, mw: 4 },
            content: {
                block: 'footer__wrapp-logo',
                mix: [{block: 'decorator', mods:{'space-v':'xl'}}, {block:'text', mods:{'align':'center'}}],
                content: {
                  block: 'logo', mods: {'width':'120px'}
                }
              }
          },

          {
              elem : 'col',
              elemMods : { sw: 6, mw: 4 },
              mix: [ {block: 'text', mods:{'align':'left-pc', 'align':'center-mob'} }, { block: 'font', mods: {'color': 'light-brown'} }],
              content:
                [
                  {block:'title', tag:'h3', mix:{block: 'text', mods:{'align':'left-mob'} }, content:'Меню'},
                  {block:'ul', tag:'ul',
                    content:[ //link link_color_light-brown link__control i-bem link_js_inited link_hovered
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Главная'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Услуги'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Практика'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Новости'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Резюме'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Филиалы'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Отзывы'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'FAQ'}},
                      {elem:'li', tag:'li', content: {block: 'link', url:'#', mods:{theme:'light-brown'}, content:'Контакты'}}
                    ]
                  }
                ]
          },

          {
              elem : 'col',
              elemMods : { sw: 6, mw: 4,},
              mix: [{block: 'text', mods:{'align':'left'}}, { block: 'font', mods: {'color': 'light-brown'} }],
              content:
              [
                { blok:'title', tag:'h3', content:'Контакты'},
                { block: 'text', content:'Контакты: ул. Земляной Вал, 38-40,стр. 5, к. 7 (м. "Курская")'},
                { block: 'link', url:'tel:+79151103333',mix:{block:'font', mods:{'color':'light-brown'}}, content:'+7 915 110-33-33'},
                { tag:'br'},
                { block: 'link', url:'mailto:order@rf-print.ru', mix:{block:'font', mods:{'color':'light-brown'}}, content:'order@rf-print.ru'},
              ]
          }
      ]
    },
    {
      block: 'row',
      content: {
        elem: 'col',
        elemMods: { sw: 12, mw: 0 },
        content: {
            block: 'footer__wrapp-logo',
            mix: [{ block:'decorator', mods:{'space-v':'xl'}}, {block:'page', mods:{'hide':'pc'}}, {block: 'text', mods:{'align':'center-mob'} }],
            content: {
              block: 'logo', mods: {'width':'120px'}
            }
          }
      }
    }
  ]
});
