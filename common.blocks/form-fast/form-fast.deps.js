({
    shouldDeps: [
      { block: 'input'},
      { block: 'input', mods:{theme:'brown'}},
      { block: 'text' },
      { block: 'button' },
      { block: 'font' },
      { block: 'form' }
    ]
})
