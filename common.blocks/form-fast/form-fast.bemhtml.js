block('form-fast')({
    mix: [{block:'theme', mods:{'color': 'bg-middle-brown' }}, {block:'decorator', mods:{'space-v':'xxxl'}}],
    js: true,
    tag:'form',
    mods : {
        'has-validation' : true,
        message : 'form-fast'
    },
    method : 'GET',
    content:
    [
      {block: 'row',
        mods: {vam: 's'},
        content:
            { elem: 'col',
              elemMods: {sw: 12}, mix: { block:'text', mods:{'align':'center'}},
              content: {block: 'font', mods:{'color':'light-brown'}, mix:[{block:'decorator', mods:{'indent-a':'auto'}},{block:'decorator', mods:{'space-b':'xl'}}], tag:'h3', content:'Не знаете какая специализация Вам нужна или остались вопросы?'}
            }
      },
      {block: 'row',
        mods: {vam: 's'},
        content:
          [
              { elem: 'col', elemMods: { sw: 12, mw: 2 }, mix:{block:'page', mods:{hide:'mob'}},
                content: {block: 'font',  mods: { size: 'm', color: 'light-brown' }, mix:{block:'decorator', mods:{'space-h':'s'}}, content:'Оставьте заявку, мы свяжемся с Вами в ближайшее время!'} },
              { elem: 'col', elemMods: { sw: 12, mw: 3 }, mix:[{block:'text', mods:{align:'center'}}],
                content: {block: 'input', mods: { theme:'brown', size: 'm'}, attrs: { required: true }, placeholder: 'Ваше имя', name: 'name', val: ''} },
              { elem: 'col', elemMods: { sw: 12, mw: 3 }, mix:[{block:'text', mods:{align:'center'}}],
                content:
                  { block: 'input',
                    js: true,
                    'has-validation' : true,
                    mods: { theme:'brown', size: 'm'},
                    attrs: { required: true },
                    placeholder: '+7 (___) ___ __ __',
                    name: 'phone',
                    val: ''
                  }
              },
              { elem: 'col', elemMods: { sw: 12, mw: 3 }, mix:{block:'text', mods:{align:'center'}},
                content:
                  { block: 'button',
                    mods: {theme:'brown', size:'m', type:'submit'},
                    mix:{block:'decorator', mods:{'indent-v':'xs'}},
                    id: 'btn-order',
                    content:
                      { block:'font',
                        mods: { size: 'auto', color: 'white' }, content:'Получить консультацию' }} }
          ]
      },
      {block: 'row',
        mods: {vam: 's'},
        mix:{block:'page', mods:{hide:'pc'}},
        content:
              { elem: 'col', elemMods: { sw: 12, mw: 2 },
                content: {block: 'font',  mods: { size: 'm', color: 'light-brown' }, mix:[{block:'text', mods:{align:'center'}}, {block:'decorator', mods:{'space-h':'s', 'space-t':'s' }}], content:'Оставьте заявку, мы свяжемся с Вами в ближайшее время!'}
              }
      }
    ]
});
