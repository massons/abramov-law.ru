block('map')({
  js: true,
  attrs: {id: 'map'},
  mods:{'black-white':true},
  content:  [
      {
        elem: 'wrapp',
        content: [
          {elem: 'cover'},
          {elem: 'map',js: true, content:''},
          {elem: 'content', content:
            [
              {block: 'text', mods:{align:'center'}, mix:{block: 'decorator', mods:{'indent-b':'xl'}}, content:{block: 'logo', mods: {'width':'120px'}} },
              {block: 'decorator', mods:{'indent-b':'xl'}, content:
                [
                  {block: 'text', content:'Телефоны:'},
                  {block: 'link', mods:{'theme':'brown'}, url:'tel:+79151103333', content:'+7 915 110-33-33'}
                ]
              },
              {block: 'decorator', mods:{'indent-b':'xl'}, content:
                [
                  {block: 'text', content:'E-mail:'},
                  {block: 'link', mods:{'theme':'brown'}, url:'mailto:info@abramov-law.ru', content:'info@abramov-law.ru'}
                ]
              },
              {block: 'text', mix:{block: 'decorator', mods:{'indent-b':'xl'}},content:'г. Москва, ул. Земляной Вал, 38-40, стр. 5, оф. 7 (м. "Курская")'},

              {block: 'text', content:'График работы:'},
              {block: 'text', content:'Пн-Пт: 9:00 - 18:00'},
              {block: 'text', content:' Сб-Вс: Выходной'}
            ]
          }
        ]
      }
  ]
})
