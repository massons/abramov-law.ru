modules.define('map', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {
        onSetMod: {
            js: {
                inited: function() {
                  /* Пустой слой для прокрутки, при клике убирается*/
                  var cover = this.findChildElem('cover');
                  this._domEvents().on('click', function() {
                    cover.domElem.remove();
                  })

                  ymaps.ready(init); /*https://yandex.ru/dev/maps/jsbox/2.1/mapparams*/

                  function init() {
                      var myMap = new ymaps.Map("map", {
                              center: [55.755257, 37.655329],
                              zoom: 15
                          }, {
                              searchControlProvider: 'yandex#search'
                          }),
                          yellowCollection = new ymaps.GeoObjectCollection(null, {
                              preset: 'islands#yellowIcon',
                              hintContent: 'Надпись, которая всплаывет при наведении на метку'
                          }),
                          yellowCoords = [[55.755257, 37.655329]];

                      for (var i = 0, l = yellowCoords.length; i < l; i++) {
                          yellowCollection.add(new ymaps.Placemark(yellowCoords[i]));
                      }


                      myMap.geoObjects.add(yellowCollection);

                      // Через коллекции можно подписываться на события дочерних элементов.
                      // yellowCollection.events.add('click', function () { alert('Кликнули по желтой метке') });

                      // Через коллекции можно задавать опции дочерним элементам.
                      // blueCollection.options.set('preset', 'islands#blueDotIcon');
                  }
            }
        }
    }}));

});
