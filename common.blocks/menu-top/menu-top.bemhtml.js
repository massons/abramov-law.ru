block('menu-top')(
    {tag: 'nav'},
    addJs()(true),
    content()(function(){

    return [
      {
          elem: 'ul', tag: 'ul',
          elemMods: {'level': 1},
          content:[
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods: { color:'light-brown', 'active':true}, content: 'Главная'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1, 'drop':'down'},
                  content: [
                      { block: 'link', url: '#1', mods: { flex: 'cente-center', color:'light-brown'},
                        content: [
                          'Услуги',
                          { block: 'icon', mods: {'arrow':'down', size:'s', 'background-size':'auto', 'correct':'menu-top'}, mix:{ block:'decorator', mods:{'space-a':'s'} }}
                        ]
                      },
                      {elem: 'ul', tag: 'ul', mix:{block:'menu-top', elem:'ul', mods:{'drop-down':'hidden'}}, content:[
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#_#1', mods: { color: 'black' }, content: 'Услуги физ лицам'} },
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#_#2', mods: { color: 'black' }, content: 'Помощь в разводе'} },
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#_#3', mods: { color: 'black' }, content: 'Возврат долга'} },
                          { elem: 'li',
                             tag: 'li',
                             elemMods: {'livel': 2 },
                             cls:'menu-top__li_drop_down',
                             content: [
                                { block: 'link', url: '#_#4', mods: { color: 'black' },
                                  content:[
                                    'Услуги' ,
                                    { block: 'icon', mods: {'arrow':'down', size:'s', 'background-size':'auto', 'correct':'menu-top'}, mix:{ block:'decorator', mods:{'space-a':'s'} }}
                                  ]
                                },
                                {elem: 'ul', tag: 'ul', cls:'menu-top__ul_drop-down_hidden', content:
                                  [
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№1', mods: { color: 'black' }, content: 'Печать на жилетах'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№2', mods: { color: 'black' }, content: 'Печать на пакетах'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№3', mods: { color: 'black' }, content: 'Печать на свитшотах'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№4', mods: { color: 'black' }, content: 'Печать на сумках'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№5', mods: { color: 'black' }, content: 'Печать на ткани'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№6', mods: { color: 'black' }, mix: { block: 'active'}, content: 'Печать на толстовках'} },
                                      { elem: 'li', tag: 'li', elemMods: {'livel': 3 }, content: { block: 'link', url: '##№7', mods: { color: 'black' }, mix: { block: 'last'}, content: 'Печать на футболках'} }
                                    ]
                                }
                              ]
                          },
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#', mods: { color: 'black' }, content: 'Сопровождение в суд'} },
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#', mods: { color: 'black' }, mix: { block: 'active'}, content: 'Нотариус'} },
                          { elem: 'li', tag: 'li', elemMods: {'livel': 2 }, content: { block: 'link', url: '#', mods: { color: 'black' }, mix: { block: 'last'}, content: 'Допрос у следователя'} }]
                      }
                  ]
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Практика'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Новости'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Резюме'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Филиалы'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Отзывы'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'FAQ'}
              },
              { elem: 'li',
                  tag: 'li',
                  elemMods: {'livel': 1},
                  content: { block: 'link', url: '#', mods:{color:'light-brown'}, content: 'Контакты'}
              }
          ]
    }
  ]
}))
