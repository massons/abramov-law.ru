modules.define('menu-top', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {
        onSetMod: {
            js: {
                inited: function() {

                    // console.log('menu JS init');
                    var _this = this;

                    var menu_level_1 = this.findChildElems({
                        elem: 'li',
                        modName: 'livel',
                        modVal: '1'
                    });

                    menu_level_1._entities.forEach(function(item, i, arr) {

                        var handlerInIN = function(e) {
                          if(arr[i].findChildElem({elem:'ul'})){
                            console.log('hover=', arr[i].findChildElem({elem:'ul'}));
                            arr[i].findChildElem({elem:'ul'}).setMod('drop-down_show');
                            arr[i].findChildElem({elem:'ul'}).delMod('drop-down_hidden');
                          }
                        };
                        $(item.domElem[0]).on("mouseenter", handlerInIN); //mouseleave

                        var handlerInOut = function(e) {
                          if(arr[i].findChildElem({elem:'ul'})){
                            console.log('hover=', arr[i].findChildElem({elem:'ul'}));
                            arr[i].findChildElem({elem:'ul'}).setMod('drop-down_hidden');
                            arr[i].findChildElem({elem:'ul'}).delMod('drop-down_show');
                          }
                        };

                        $(item.domElem[0]).on("mouseleave", handlerInOut);

                    });

                }
            }
        }
    }))

});
