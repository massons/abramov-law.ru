modules.define('form', ['i-bem-dom'], function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {
    onSetMod: {
        js: {
            inited: function() {


                this._form = this;
                this._form._domEvents().on('submit', function(e, val) {
                    this._form.validate()
                        .then(function (fieldsStatuses) {
                            if(this._form.checkFields(fieldsStatuses)) {
                                this._form.getMessage().hide();
                                console.log(val);
                            } else {
                                this._form.setMessageVal(this._concatMessages(fieldsStatuses));
                                this._form.getMessage().show();
                                this._form.getInvalidFields()
                                    .then(function (invalidFields) {
                                        invalidFields[0].getControl().setMod('focused');
                                    });
                            }
                        }.bind(this));
                }.bind(this));



            }
        }
    }
}));

});
