block('header')({
  mix: [{ block:"page", mods:{'bg-gradient': 'brown' } }, {block:'font'}],
  content:  [
        {
          block: 'row',
          mods: {vam: 's'},
          content: [
            {
              elem: 'col',
              elemMods: {sw: 3},
              content: { block: 'header', elem:'wrapp-logo', content: { block: 'logo', mods: {'width':'200px'} } }
            },
            {
              elem: 'col',
              elemMods: {sw: 9},
              content: [
                { block: 'header', elem:'info'},
                { block: 'menu-top', mix: { block:'page', mods:{'hide':'mob'}} },

                { block: 'site-overlay', content: '' },
                { block: 'pushy', mix: { block: 'pushy-right' }}
              ]
            }]
        }]
});
