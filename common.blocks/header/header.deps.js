({
    shouldDeps: [
        { block: 'decorator'},
        { block: 'button', mods: {theme: 'islands'} },
        { block: 'form'},
        { block: 'font'},
        { block: 'header', elem:'info'},
        { block: 'menu-top' },
        { block: 'logo' },
        { block: 'lost'},
        { block: 'modal', mods: { autoclosable: true, theme: 'islands'} },
        { block: 'icon'},
        { block: 'page'},
        { block: 'page', mods: {hide:'pc', hide:'mob'} },
        { block: 'row' }

    ]
})
