block('header').elem('info').content()(function() {
  return [
    {
    block : 'row',
    content : [
        {
            elem : 'col',
            elemMods : { sw: 0, mw: 4 },
            mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}},{block:'font', mods:{'size':'m'}}],
            content : { block:'link', mix: { block: 'font', mods: {'color': 'light-brown'} }, url:'mailto:Info@abramov-law.ru', content:'Info@abramov-law.ru'}
        },
        {
            elem : 'col',
            elemMods : { sw: 0, mw: 4 },
            mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}, { block: 'font', mods: {'color': 'light-brown'} },{block:'font', mods:{'size':'m'}}],
            content: {html:'г. Москва<br> ул. Земляной Вал, 38-40,<br> стр. 5, к. 7 (м. "Курская")'}
        },
        {
            elem : 'col',
            elemMods : { sw: 0, mw: 4,},
            mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}, { block: 'font', mods: {'color': 'light-brown'} }],
            content:
            [
              {block:'link', mods: {cursor:'pointer'}, mix:[{block: 'font', mods: {'color': 'light-brown'}}, {block: 'font', mods: {'size': 'xxl-auto'}}, {block: 'text', mods: {'decoration': 'none'}} ], url:'tel:+79151103333', content:'+7 915 110-33-33'},
              {tag:'br', mix:{block:'page', mods:{'hide':'mob'}}},
              {block:'text',tag:'small', mix:[{block:'page', mods:{'hide':'mob'}}, {block:'font', mods:{'color': 'light-brown'}}], content:'Ежедневно с 9:00 до 18:00'}
            ]
        }
    ]
  },
  {
    block:'row',
    content: [
      {
          elem : 'col',
          elemMods : { sw: 8, mw: 0, lw: 0, xl: 0, xxl: 0},
          mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}, { block: 'font', mods: {'color': 'light-brown'} }, {block:'page', mods:{'hide':'pc'}}],
          content:
          [
            {block:'link', mods: {cursor:'pointer'}, mix:[{block: 'font', mods: {'color': 'light-brown'}}, {block: 'font', mods: {'size': 'xl'}}, {block: 'text', mods: {'decoration': 'none'}} ], url:'tel:+79151103333', content:'+7 915 110-33-33'},
            {tag:'br', mix:{block:'page', mods:{'hide':'mob'}}},
            {block:'text',tag:'small', mix:[{block:'page', mods:{'hide':'mob'}}, {block:'font', mods:{'color': 'light-brown'}}], content:'Ежедневно с 9:00 до 18:00'}
          ]
      },
      {
        elem: 'col',
        elemMods: { sw: 4, mw: 0, lw: 0, xl: 0, xxl: 0},
        mix:[{block: 'text', mods:{'align':'center'}}, {block: 'decorator', mods:{'space-v':'xs'}}, { block: 'font', mods: {'color': 'light-brown'}}, {block:'page', mods:{'hide':'pc'}} ],
        content:[
              {
                block: 'link',
                mix: [{ block: 'pushy-link' }, { block: 'page_hide_pc' }],
                content:
                    { block: 'icon', mods: {'burger':true, 'background-size':'auto', size:'xs'}, mix:{ block:'decorator', mods:{'space-a':'s'} }}
              }
            ]

      }
    ]
  }
 ]
});
