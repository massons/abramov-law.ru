block('category')(addJs()(true),addMix()({block:'content'}),content()(function(){

return [
  { block: 'row', mods: {vam: 's'},content:{ elem: 'col',elemMods: { sw: 12 },content:{block:'title', tag:'h1', content:'ОБЛАСТИ ДЕЯТЕЛЬНОСТИ'}}},
  { block: 'row', mods: {vam: 's'},content:{ elem: 'col',elemMods: { sw: 12 },content:{block:'category', elem:'buttons', mix:{block:'page', mods:{hide:'pc'}} }} },

  { block: 'row', mods: {vam: 's'},content:{ elem: 'col',elemMods: { sw: 12 },content:{block:'category', elem:'fiz-title', tag:'h2', content:'Физическим лицам'}}},
  { block: 'row',
    mods: {vam: 's'},
    content:
      [
        { elem: 'col',
          elemMods: { sw: 12, mw: 4  },
          mix:{block:'page', mods:{hide:'mob'}},
          content: {block:'image', url:'../../img/cat_fiz.jpg'},
        },
        { elem: 'col',
          elemMods: { sw: 12, mw: 8 },
          content:{block:'category', elem:'fiz'}
        }
      ]
  },

  {block: 'row', mods: {vam: 's'}, content: { elem: 'col', elemMods: { sw: 12 }, content:{block:'category', elem:'ur-title', elemMods:{'hide':'mob'}, tag:'h2', content:'Юридическим лицам'}}
  },
  { block: 'row',
    mods: {vam: 's'},
    content:
      [
        { elem: 'col',
          elemMods: { sw: 12, mw: 4 },
          mix:{block:'page', mods:{hide:'mob'}},
          content: {block:'image', url:'../../img/cat_ur.jpg'},
        },
        { elem: 'col',
          elemMods: { sw: 12, mw: 8 },
          content:{block:'category', elem:'ur'}
        }
      ]
  }
  ]
}));
