block('category__buttons')(content()(function(){

return [

    { block: 'control-group',
      mix:{block:'text', mods:{align:'center'}},
      content: [
        { block:'button', mods:{'theme':'brown', size:'xl', checked:true}, mix:[{block:'font', mods:{'weight':'bold'}},{block:'decorator',mods:{'indent-a':'s'}}], content:'ДЛЯ ФИЗИЧЕСКИХ ЛИЦ'},
        { block:'button', mods:{'theme':'brown', size:'xl', 'active':true}, mix:[{block:'font', mods:{'weight':'bold'}},{block:'decorator',mods:{'indent-a':'s'}}], content:'ДЛЯ ЮРИДИЧЕСКИХ ЛИЦ'},
      ]
    }
  ]

}));
