modules.define('category', ['i-bem-dom','button'], function(provide, bemDom, Button) {

    provide(bemDom.declBlock(this.name, {
          onSetMod: {
              js: {
                  inited: function() {
                      // Finded all items
                      var btn_fiz = this.findChildBlocks(Button)._entities[0];
                      var btn_ur = this.findChildBlocks(Button)._entities[1];
                      var fiz = this.findChildElem({ elem:'fiz'});
                      var ur = this.findChildElem({ elem:'ur'});
                      var title_fiz = this.findChildElem({ elem:'fiz-title'});
                      var title_ur = this.findChildElem({ elem:'ur-title'});


                      btn_ur._domEvents().on('click', function() {

                          if(!ur.hasMod('checked')){

                              title_ur.delMod('hide_mob');
                              btn_ur.setMod('checked');
                              ur.delMod('hide_mob');

                              title_fiz.setMod('hide_mob');
                              btn_fiz.delMod('checked');
                              fiz.setMod('hide_mob');



                           }
                      });

                      btn_fiz._domEvents().on('click', function() {
                          if(!fiz.hasMod('checked')){

                              title_fiz.delMod('hide_mob');
                              btn_fiz.setMod('checked');
                              fiz.delMod('hide_mob');

                              title_ur.setMod('hide_mob');
                              btn_ur.delMod('checked');
                              ur.setMod('hide_mob');
                           }
                      });

                  }
              }
          }
        }
      )
    )

});
