({
    shouldDeps: [
      { block: 'icon' },
      { block: 'image' },
      { block: 'row' },
      { block: 'link', mods:{'theme':'brown'} },
      { block: 'page', mods:{'display':'none'} },
      { block: 'font', mods:{'weight':'bold'} },
      { block: 'radio-group' },
      { block: 'button' },
      { block: 'decorator' },
      { block: 'category', elem:'fiz' },
      { block: 'category', elem:'ur' },
      { block: 'category', elem:'buttons' }
    ]
})
