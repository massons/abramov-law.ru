block('breadcrumbs')({
  mix: [{ block:'decorator', mods:{'space-v':'xl'}}],
  content:
  [
    {
      block: 'row',
      content: {
        elem: 'col',
        elemMods: { sw: 12, mw: 12, lw: 12, xl: 12, xxl: 12},
        content: [
          {
            block: 'link',
            url:'#',
            content: 'Главная'
          },
          '/',
          {
            block: 'link',
            url:'#',
            content: 'Контакты'
          }
        ]
      }
    }
  ]
});
