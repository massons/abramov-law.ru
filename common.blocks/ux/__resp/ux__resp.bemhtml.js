block('ux').elem('resp').content()(function() {
  return [{
    block : 'row',
    content : [
      {
          elem : 'col',
          elemMods : { sw: 0, mw: 4 }, //lw: 12, xl: 12, xxl: 6
          mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}],
          content : { block:'link', mix: { block: 'font', mods: {'color': 'light-brown'} }, url:'mailto:Info@abramov-law.ru', content:'fffffffffffffffffffff@abramov-law.ru'}
      },
    ]
  }]
})
