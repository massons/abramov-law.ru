block('ux')(
  addJs()(true),
  content()(function() {
  return [{
    elem: 'wrapp',
    content:[
     {tag:'hr'},
     {
         block : 'button',
         mods : { theme : 'brown', size : 'l', togglable : 'check'},
         text : 'Бесплатная консультация'
     },
     {html:'<br><br>Другой шрифт<b>Bold</b> <b><i>Bold italic</i></b> test text<br><br>'},
     { block: 'text', content:'Другой шрифт, мазафака <b>Bold</b> <b><i>Bold italic</i></b> test text<br><br> ОБЛАСТИ ДЕЯТЕЛЬНОСТИ',
      mix: [
          { block:'page', mods: {'theme':'brown-font-Cormorant'}},
          { block:'font', mods: [{'size':'xxl'} ]  },
          { block:'decorator', mods:{'space-a': 'xxxl'}}
        ]
     },
     
     {tag:'br'},
     {tag:'br'}
    ]

  },
  {tag:'hr'},
  {block:'ux', elem:'resp'},
  {tag:'hr'},
  {tag:'hr'},
  {
      block: 'icon',
      content: {
          tag: 'svg',
          cls: 'action_type_download',
          attrs: { xmlns: '../img/icon/ur/ico_ur_arbitrazhnye_sudy.svg', width: 16, height: 16 },
          content: '<path d="M1 13v2h14v-2h-14zm13-7h-3v-5h-6v5.031l-3-.031 6 6 6-6z"/>'
      }
  },
  {tag:'hr'},
  {tag:'hr'},
  {tag:'hr'},
  {block:'icon', mods:{'ico-ur-arbitrazhnye-sudy':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-arendnye-spory':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-dogovornye-spory':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-ispolnitelnoe-proizvodstvo':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-kadastrovaya-stoimost':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-osparivanie-dejstvij-admin':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-podgotovka-korp-dokumentov':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-soprovozhdenie-bankrotstva':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-soprovozhdenie-nalog-proverok':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-soprovozhdenie-sdelok':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-subsidiarnaya-otvetstvennost':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-vzyskanie-debit-zadolzhennosti':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-yur-soprovozhdenie':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-zashchita-biznesa':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-zashchita-pri-proverkah':true},cls : 'icon-url' },
  {block:'icon', mods:{'ico-ur-zashchita-reputacii':true},cls : 'icon-url' },
  {tag:'hr'},
  {tag:'hr'},
  {block:'icon', mods:{'ico-fiz-avtoyurist':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-bankrotstvo':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-dolevoe-stroitelstvo':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-ehkonomicheskie-prestupleniya':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-grazhdanskie-spory':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-ispolnitelnoe-proizvodstvo':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-konsultacii-advokata':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-nasledstvennye-spory':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-sdelki-s-nedvizhimostyu':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-semejnye-spory':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-sostavlenie-iskov':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-trudovye-spory':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-zashchita-prav-potrebitelej':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-zemelnye-spory':true}, cls : 'icon-url' },
  {block:'icon', mods:{'ico-fiz-zhilishchnye-spory':true}, cls : 'icon-url' },

  {tag:'hr'},
  {tag:'hr'},
  {
    block : 'row',
    content : [
      {
          elem : 'col',
          elemMods : { sw: 2, mw: 4 }, //lw: 12, xl: 12, xxl: 6
          mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}],
          content : { block:'link', mix: { block: 'font', mods: {'color': 'light-brown'} }, url:'mailto:Info@abramov-law.ru', content:'fffffffffffffffffffff@abramov-law.ru'}
      },
      {
          elem : 'col',
          elemMods : { sw: 0, mw: 2 }, //lw: 12, xl: 12, xxl: 6
          mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}],
          content : { block:'link', mix: { block: 'font', mods: {'color': 'light-brown'} }, url:'mailto:Info@abramov-law.ru', content:'fffffffffffffffffffff@abramov-law.ru'}
      },
      {
          elem : 'col',
          elemMods : { sw: 1, mw: 0 }, //lw: 12, xl: 12, xxl: 6
          mix: [{block: 'text', mods:{'align':'right'}}, {block: 'decorator', mods:{'space-v':'xs'}}],
          content : { block:'link', mix: { block: 'font', mods: {'color': 'light-brown'} }, url:'mailto:Info@abramov-law.ru', content:'fffffffffffffffffffff@abramov-law.ru'}
      },
    ]
  }
]
}));
