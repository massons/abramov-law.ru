modules.define('ux', ['i-bem-dom'], function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {
    onSetMod: {
        js: {
            inited: function() {
                  console.log('ux JS init:');
                  //ul__li_drop_down
                  var find_dr_d_m = this.findChildElem({ elem:'li', modName:'drop', modVal : 'down' });
                  console.log(this,find_dr_d_m);
            }
        }
    }
}));

});
