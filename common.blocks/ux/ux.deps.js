({
    shouldDeps: [
      { block: 'image'},
      { block: 'row', elem: 'col'},
      { block: 'header', elem:'info'},
      { block: 'decorator'},
      { block: 'icon'},
      { block: 'icon', mods: {action: 'download'}},
      { block: 'icon', mods: {'ico-ur-arbitrazhnye-sudy': true}},
      { block: 'button'}
     ]
})
