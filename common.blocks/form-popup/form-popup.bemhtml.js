block('form-popup')({
    mix: [{block:'theme', mods:{'color': 'bg-middle-brown' }}, {block:'decorator', mods:{'space-a':'xxxl'}}],
    js: true,
    tag:'form',
    mods : {
        'has-validation' : true,
        message : 'form-popup'
    },
    method : 'GET',
    content:
    [
      { block: 'text', mods: {align:'cente'}, mix:[{block:'font', mods:{color:'light-brown'}}], content: 'Записаться на бесплатную консультацию!'},
      { block: 'input', mods: { theme:'brown', size: 'm'}, attrs: { required: true }, placeholder: 'Ваше имя', name: 'name', val: ''} ,
      { block: 'input',
        js: true,
        'has-validation' : true,
        mods: { theme:'brown', size: 'm'},
        attrs: { required: true },
        placeholder: '+7 (___) ___ __ __',
        name: 'phone',
        val: ''
      },
      { block: 'button',
        mods: {theme:'brown', size:'m', type:'submit'},
        mix: {block:'decorator', mods:{'indent-v':'xs'}},
        id: 'btn-order',
        content:
          { block: 'font',
            mods: { size: 'auto', color: 'white' }, content:'Отправить' }
      }
    ]
});
