modules.define('input', ['i-bem-dom', 'jquery'], function(provide, bemDom, $, FormField) {

  /* borschik:include:../../node_modules/jquery-mask-plugin/dist/jquery.mask.min.js */

  provide(bemDom.declBlock(this.name, {

    onSetMod: {
        js: {
            inited: function() {

              //найти все input phone and add mask. setMod no valid
              var _input = this.findChildElem('control');

              if(_input.domElem.attr('name') == 'phone'){
                _input.domElem.mask('+7 (000) 000-00-00', {
                  placeholder: '+7 (   ) ___-__-__'
                });
                this.setMod('no-valid');
              }

              //найти все input phone and add mask
              var _email = this.findChildElem('control');
              if(_email.domElem.attr('name') == 'mail'){
                // _email //https://github.com/RobinHerbots/Inputmask
                // _email.inputmask({
                //   mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
                //   greedy: false,
                //   onBeforePaste: function (pastedValue, opts) {
                //     pastedValue = pastedValue.toLowerCase();
                //     return pastedValue.replace("mailto:", "");
                //   },
                //   definitions: {
                //     '*': {
                //       validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                //       casing: "lower"
                //     }
                //   }
                // });
              }
            }
        },
        focused: function () {

            //найти все input
            var _input = this.findChildElem('control');
            //valid just Phone
            if(_input.domElem.attr('name') == 'phone'){
              var _val = _input.domElem.val()
              if(_val.length == 18){
                this.setMod('valid');
                this.delMod('novalid');
              }else{
                this.setMod('novalid');
                this.delMod('valid');
              }
            }

        }
    }

  }));

});
